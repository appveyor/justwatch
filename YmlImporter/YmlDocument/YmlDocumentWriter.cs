﻿using System;
using System.Globalization;
using System.Xml;

namespace Justwatch.YmlImporter.YmlDocument
{
    public class YmlDocumentWriter : IDisposable
    {
        private readonly XmlWriter writer;

        public YmlDocumentWriter(string fileName)
        {
             var xmlWriterSettings = new XmlWriterSettings()
              {
                Indent = true,
                IndentChars = "\t"
              };

            writer = XmlWriter.Create(fileName, xmlWriterSettings);
        }

        public void WriteHeader()
        {
            writer.WriteDocType("yml_catalog", null, "shops.dtd", null);
            writer.WriteStartElement("yml_catalog");
            writer.WriteAttributeString("date", DateTime.Now.ToString("yyyy-MM-dd HH-mm"));

            writer.WriteStartElement("shop");

                writer.WriteElementString("name", "justwatch.ru");
                writer.WriteElementString("company", "justwatch.ru");
                writer.WriteElementString("url", "http://www.ebay.com/usr/justwatch.ru");

                writer.WriteStartElement("currencies");
                    writer.WriteStartElement("currency");
                    writer.WriteAttributeString("id", "RUR");
                    writer.WriteAttributeString("rate", "1");
                    writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteStartElement("categories");
                    writer.WriteStartElement("category");
                    writer.WriteAttributeString("id", "14324");
                    writer.WriteString("Купить оригинальные наручные часы");
                    writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteStartElement("offers");

            writer.Flush();
        }

        public void WriteOffer(OfferRecord offerRecord)
        {
            writer.WriteStartElement("offer");
                writer.WriteAttributeString("id", offerRecord.Id);
                writer.WriteAttributeString("available", offerRecord.Available);
            
                writer.WriteElementString("price", ((int)offerRecord.Price).ToString(CultureInfo.InvariantCulture));
                writer.WriteElementString("currencyId", offerRecord.Currency);
                writer.WriteElementString("categoryId", offerRecord.Category);

                foreach (var picture in offerRecord.Pictures)
                {
                    writer.WriteElementString("picture", picture);
                }
               
                writer.WriteElementString("name", offerRecord.Name);
                var description = string.Format("<![CDATA[{0}]]>", offerRecord.Description);
                writer.WriteStartElement("description");
                    writer.WriteRaw(description);
                writer.WriteEndElement();

                foreach (var parameter in offerRecord.Parameters)
                {
                    writer.WriteStartElement("param");
                    writer.WriteAttributeString("name", parameter.Key);
                    writer.WriteString(parameter.Value);
                    writer.WriteEndElement();
                }

                writer.WriteStartElement("stock");
                writer.WriteString(offerRecord.Stock.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();

            writer.WriteEndElement();
        }

        public void WriteFooter()
        {
                    writer.WriteEndElement();
                writer.WriteEndElement();
            writer.WriteEndElement();

            writer.Flush();
        }

        public void Dispose()
        {
            writer.Close();
        }
    }
}
