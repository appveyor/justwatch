﻿using System.Collections.Generic;

namespace Justwatch.YmlImporter.YmlDocument
{
    public class OfferRecord
    {
        public OfferRecord()
        {
            Parameters = new Dictionary<string, string>();
        }

        public string Id { get; set; }
        public string Available { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public string Category { get; set; }
        public string[] Pictures { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> Parameters { get; private set; }
        public int Stock { get; set; }
    }
}
