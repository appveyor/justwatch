﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter
{
    public class DescriptionProvider
    {
        private readonly string descriptionTemplate;

        public DescriptionProvider(string description)
        {
            this.descriptionTemplate = description;
        }

        public string GetDescription(Dictionary<string, string> tags)
        {
            var description = descriptionTemplate;

            foreach (var tag in tags)
            {
                description = description.Replace("$" + tag.Key + "$", tag.Value);
            }

            return description;
        }
    }
}
