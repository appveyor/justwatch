﻿using Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter.Pricing
{
    class PricingReader
    {
        public Dictionary<string, string> ReadBrandFormulas(string fileName)
        {
            var brandFormulas = new Dictionary<string, string>();

            using (var fileStream = File.OpenRead(fileName))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    excelReader.Read();

                    while (excelReader.Read())
                    {
                        var brand = excelReader.GetString(0);
                        var formula = excelReader.GetString(1);

                        brandFormulas.Add(brand, formula);
                    }
                }
            }

            return brandFormulas;
        }
    }
}
