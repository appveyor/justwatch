﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter.Pricing
{
    class PriceCalculator
    {
        private readonly Dictionary<string, Func<double, double>> brandFunctions = 
            new Dictionary<string, Func<double, double>>();

        private readonly DelegatesGenerator delegatesGenerator = new DelegatesGenerator();

        public void LoadBrandFormulas(Dictionary<string, string> brandFormulas)
        {
            var functions = delegatesGenerator.GenerateDelegates(brandFormulas.Values.ToArray());

            for (int i = 0; i < functions.Length; i++)
                brandFunctions.Add(brandFormulas.Keys.ElementAt(i).ToUpper(), functions[i]);
        }

        public double? CalculatePrice(string brand, double originalPrice)
        {
            if (brand == null)
                return null;

            brand = brand.ToUpper();

            if (!brandFunctions.ContainsKey(brand))
                return null;

            var function = brandFunctions[brand];

            return function(originalPrice);
        }
    }
}
