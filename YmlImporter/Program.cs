﻿using Justwatch.YmlImporter.Interfaces;
using Justwatch.YmlImporter.Pricing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

using DeltaWatchProvider = Justwatch.YmlImporter.Delta.WatchProvider;
using BestWatchProvider = Justwatch.YmlImporter.BestWatch.WatchProvider;
using Justwatch.YmlImporter.Delta;

namespace Justwatch.YmlImporter
{
    public class Program
    {  
        static void Main(string[] args)
        {  
            var currentDirectory = Directory.GetCurrentDirectory();

            try
            {
                var parametersParser = new Console.ImportParametersParser();
                var parameters = parametersParser.Parse(args);

                var ymlDocumentPath = Path.Combine(currentDirectory, DateTime.Now.ToString("ddMMpriceHHmm") + ".yml");
                
                var ymlImporter = CreateYmlImporter();

                ymlImporter.MaxPrice = parameters.MaxPrice;
                if (parameters.Stock.HasValue)
                    ymlImporter.Stock = parameters.Stock.Value;

                var deltaWatchProvider = CreateDeltaWatchProvider();
                var bestWatchProvider = CreateBestWatchProvider();
                
                System.Console.WriteLine("Импортируем yml документ...");
                
                ymlImporter.Import(
                    new CompositeWatchProvider(deltaWatchProvider, bestWatchProvider),
                    ymlDocumentPath);

                System.Console.WriteLine("Документ создан.");

                ValidateYmlSchema(ymlDocumentPath);

                var productsWithoutDetails = deltaWatchProvider.ProductsWithoutDetails.
                    Concat(bestWatchProvider.ProductsWithoutDetails).ToArray();

                DisplayImportStatus(
                    ymlImporter.ImportedOffers,
                    ymlImporter.ProductsWithoutName.Count,
                    productsWithoutDetails.Length,
                    ymlImporter.ProductsWithoutPicture.Count,
                    ymlImporter.ProductsWithHigherPrice.Count,
                    ymlImporter.BrandsWithoutCalculatedPrice.Count);

                SaveImportStatus(
                   ymlImporter.ProductsWithoutName,
                   productsWithoutDetails,
                   ymlImporter.ProductsWithoutPicture,
                   ymlImporter.BrandsWithoutCalculatedPrice);

                System.Console.WriteLine("Нажмите любую клавишу для выхода...");
                System.Console.ReadKey();
            }
            catch (Exception ex)
            {
                var errorFilePath = Path.Combine(currentDirectory, ApplicationSettings.Error);
                File.WriteAllText(errorFilePath, ex.Message + ex.StackTrace);

                System.Console.WriteLine(ex.Message);
                System.Console.ReadKey();
            }
        }

        private static DeltaWatchProvider CreateDeltaWatchProvider()
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            var productsDocumentPath = Path.Combine(currentDirectory, ApplicationSettings.DeltaProducts);

            var pricesFolder = Path.Combine(currentDirectory, ApplicationSettings.DeltaPrices);

            var deltaWatchProvider = new DeltaWatchProvider(
                new PricesReader(pricesFolder),
                new WatchRepository(new ProductsReader(productsDocumentPath)));

            return deltaWatchProvider;
        }

        private static BestWatchProvider CreateBestWatchProvider()
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            var productsDocumentPath1 = Path.Combine(currentDirectory, ApplicationSettings.BestWatchProducts);
            var productsDocumentPath2 = Path.Combine(currentDirectory, ApplicationSettings.DeltaProducts);

            return new BestWatchProvider(
                new Justwatch.YmlImporter.BestWatch.ProductsDocumentReader(productsDocumentPath1),
                new WatchRepository(new ProductsReader(productsDocumentPath2)));
        }

        private static string GetCompanyDescription()
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            var russianEncoding = Encoding.GetEncoding(1251);
            var descriptionFilePath = Path.Combine(currentDirectory, ApplicationSettings.Description);
            return File.ReadAllText(descriptionFilePath, russianEncoding);
        }

        private static void ValidateYmlSchema(string ymlDocumentPath)
        {
            var schemaValidator = new DtdSchemaValidator();
            var validationResult = schemaValidator.Validate(ymlDocumentPath);
            if (validationResult.Errors.Count > 0)
            {
                File.Delete(ymlDocumentPath);
                var messageBuilder = new StringBuilder();
                messageBuilder.AppendLine("Схема yml документа некорректна.");
                foreach (var error in validationResult.Errors)
                    messageBuilder.AppendLine(error);
                throw new Exception(messageBuilder.ToString());
            }
            else
            {
                System.Console.WriteLine("Схема yml документа корректна.");
            }
        }

        private static void SaveImportStatus(
            IEnumerable<string> productsWithoutName, 
            IEnumerable<string> productsWithoutDetails, 
            IEnumerable<string> productsWithoutPicture,
            IEnumerable<string> productsWithoutCalculatedPrice)
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            var productsWithoutNamePath = Path.Combine(currentDirectory, ApplicationSettings.ProductsWithoutName);
            File.WriteAllLines(productsWithoutNamePath, productsWithoutName);

            var productsWithoutDetailsPath = Path.Combine(currentDirectory, ApplicationSettings.ProductsWithoutDetails);
            File.WriteAllLines(productsWithoutDetailsPath, productsWithoutDetails);

            var productsWithoutPicturePath = Path.Combine(currentDirectory, ApplicationSettings.ProductsWithoutPicture);
            File.WriteAllLines(productsWithoutPicturePath, productsWithoutPicture);

            var productsWithoutCalculatedPricePath = Path.Combine(currentDirectory, ApplicationSettings.BrandsWithoutFormula);
            File.WriteAllLines(productsWithoutCalculatedPricePath, productsWithoutCalculatedPrice);
        }

        private static void DisplayImportStatus(
            int importedOffers, 
            int productsWithoutName, 
            int productsWithoutDetails, 
            int productsWithoutPicture, 
            int productsWithHigherPrice, 
            int productsWithoutCalculatedPrice)
        {
            System.Console.WriteLine("Импортировано товаров: {0}", importedOffers);
            System.Console.WriteLine("Товаров без имени: {0}", productsWithoutName);
            System.Console.WriteLine("Товаров без описания: {0}", productsWithoutDetails);
            System.Console.WriteLine("Товаров без картинки: {0}", productsWithoutPicture);
            System.Console.WriteLine("Товаров с ценой выше максимума: {0}", productsWithHigherPrice);
            System.Console.WriteLine("Товаров без рассчитанной цены: {0}", productsWithoutCalculatedPrice);
        }

        private static YmlImporter CreateYmlImporter()
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            var pricingFilePath = Path.Combine(currentDirectory, ApplicationSettings.Pricing);
            var pricingReader = new PricingReader();
            var brandFormulas = pricingReader.ReadBrandFormulas(pricingFilePath);

            var companyDescription = GetCompanyDescription();
            var brandPictureRepository = new BrandPictureRepository(Path.Combine(currentDirectory, ApplicationSettings.BrandPictures));

            return new YmlImporter(brandFormulas, companyDescription, brandPictureRepository);
        }
    }
}
