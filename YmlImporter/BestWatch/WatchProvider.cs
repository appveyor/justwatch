﻿using System.Collections.Generic;
using System.Linq;
using Justwatch.YmlImporter.Domain;
using System;
using Justwatch.YmlImporter.Delta;
using System.Collections.ObjectModel;
using Justwatch.YmlImporter.Utilities;

namespace Justwatch.YmlImporter.BestWatch
{
    public class WatchProvider : IWatchProvider
    {
        private readonly ProductsDocumentReader productsReader;
        private readonly WatchRepository watchRepository;

        public WatchProvider(ProductsDocumentReader productsReader, WatchRepository watchRepository)
        {
            this.productsReader = productsReader;
            this.watchRepository = watchRepository;

            ProductsWithoutDetails = new Collection<string>();
        }

        public Collection<string> ProductsWithoutDetails { get; private set; }

        public IEnumerable<Watch> ImportWatches()
        {
            return productsReader.ReadProducts().Where(product => CheckProduct(product)).Select(product => CreateWatch(product)).Select(watch => MergeWatch(watch));
        }

        private bool CheckProduct(Product product)
        {
            if (product.Type != "Часы")
                return false;

            if (product.Mechanism == "Термометр")
                return false;

            return true;
        }

        private Watch CreateWatch(Product product)
        {
            var watch = new Watch();
            watch.Article = product.Articul;
            watch.Model = product.Articul;
            watch.Price = product.Price;
            watch.Pictures = new string[] { product.Image };
            watch.Name = string.Format("Оригинальные наручные часы {0} {1} {2}", product.Brand.ToFirstLetterUppercase(), product.Articul, product.Sex);
            watch.Brand = product.Brand.ToFirstLetterUppercase();
            watch.Sex = ParseWatchSex(product.Sex);
            watch.Mechanism = ParseWatchMechanism(product.Mechanism);
            watch.Watertight = !string.IsNullOrEmpty(product.Waterproof);
            watch.Waterproof = product.Waterproof;
            watch.BoxMaterial = product.Material;
            watch.Glass = product.Glass;
            watch.Diameter = product.Width;
            watch.Depth = product.Depth;

            return watch;
        }

        private Watch MergeWatch(Watch watch)
        {
            var watchFromRepository = watchRepository.GetWatch(watch.Brand, watch.Model);
            if (watchFromRepository == null)
            {
                ProductsWithoutDetails.Add(string.Format("{0} {1}", watch.Brand, watch.Model));
                return watch;
            }

            return watchFromRepository.Merge(watch);
        }

        private WatchSex ParseWatchSex(string sex)
        {
            switch (sex)
            {
                case "Мужские":
                    return WatchSex.Male;
                case "Женские":
                    return WatchSex.Female;
                case "Унисекс":
                    return WatchSex.Unisex;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить пол для часов: \"{0}\".", sex));
            }
        }

        private WatchMechanism ParseWatchMechanism(string mechanism)
        {
            switch (mechanism)
            {
                case "Электронные часы":
                case "Кварцевый":
                case "Кварцевый хронограф":
                case "Кварцевые часы":
                    return WatchMechanism.Quartz;
                case "Механические часы с автоподзаводом":
                case "Механический хронограф с автоподзаводом":
                case "Механический хронометр с автоподзаводом":
                    return WatchMechanism.MechanicalAutomaticWindUp;
                case "Механические часы с ручным заводом":
                    return WatchMechanism.MechanicalManualWindUp;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип механизма часов: \"{0}\".", mechanism));
            }
        }
    }
}
