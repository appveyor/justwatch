﻿using Justwatch.YmlImporter.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Justwatch.YmlImporter.Delta
{
    public class WatchProvider : IWatchProvider
    {
        private readonly PricesReader pricesReader;
        private readonly WatchRepository watchRepository;

        public WatchProvider(PricesReader pricesReader, WatchRepository watchRepository)
        {
            this.pricesReader = pricesReader;
            this.watchRepository = watchRepository;

            ProductsWithoutDetails = new Collection<string>();
        }

        public Collection<string> ProductsWithoutDetails { get; private set; }

        public IEnumerable<Watch> ImportWatches()
        {
            foreach (var priceRecord in pricesReader.ReadPrices())
            {
                var watch = watchRepository.GetWatch(priceRecord.Brand, priceRecord.Model);
                if (watch == null)
                {
                    ProductsWithoutDetails.Add(string.Format("{0} {1}", priceRecord.Brand, priceRecord.Model));
                    continue;
                }

                watch.Price = priceRecord.Price;

                yield return watch;
            }
        }
    }
}
