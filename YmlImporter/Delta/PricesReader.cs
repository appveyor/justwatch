﻿using System.Collections.ObjectModel;
using System.IO;
using Excel;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Justwatch.YmlImporter.Delta
{
    public class PricesReader
    {
        private static readonly string RublesPostFix = " руб.";

        private readonly string pricesFolder;

        public PricesReader(string pricesFolder)
        {
            this.pricesFolder = pricesFolder;
        }

        public IEnumerable<PriceRecord> ReadPrices()
        {
            var pricesDocumentsPaths = Directory.GetFiles(pricesFolder, "*.xls");

            var pricesRecords = new Collection<PriceRecord>();

            foreach (var fileName in pricesDocumentsPaths)
            {
                var brand = GetShortBrandName(fileName);

                using (var fileStream = File.OpenRead(fileName))
                {
                    using (var excelReader = ExcelReaderFactory.CreateBinaryReader(fileStream))
                    {
                        while (excelReader.Read())
                        {
                            var model = excelReader.GetString(0);
                            var priceString = excelReader.GetString(1);
                            if (priceString == null)
                                continue;

                            priceString = priceString.Replace(RublesPostFix, string.Empty);

                            double price;
                            if (!double.TryParse(priceString, out price))
                                continue;

                            var priceRecord = new PriceRecord { Model = model, Price = price, Brand = brand };
                            pricesRecords.Add(priceRecord);
                        }
                    }
                }
            }

            return pricesRecords;
        }

        private string GetShortBrandName(string fileName)
        {
            var pricesDocumentName = Path.GetFileNameWithoutExtension(fileName);
            if (pricesDocumentName != null)
                return new string(pricesDocumentName.TakeWhile(@char => @char != '_').ToArray()).ToUpper();

            throw new Exception("Can't get brand name.");
        }
    }
}
