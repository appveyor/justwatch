namespace Justwatch.YmlImporter.Delta
{
    public class ProductRecord
    {
        public string Article { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string[] Pictures { get; set; }
        public string Brand { get; set; }
        public string Sex { get; set; }
        public string MechanismType { get; set; }
        public string Digits { get; set; }
        public string Watertight { get; set; }
        public string WR { get; set; }
        public string BoxMaterial { get; set; }
        public string StrapMaterial { get; set; }
        public string Glass { get; set; }
        public string TimeDisplayType { get; set; }
        public string EnergySource { get; set; }
        public string Diameter { get; set; }
        public string Weight { get; set; }
        public string DateDisplay { get; set; }
        public string AdditionalFunctions { get; set; }	
        public string Warranty { get; set; }
    }
}