﻿using System;
using System.IO;
using Excel;
using System.Collections.Generic;

namespace Justwatch.YmlImporter.Delta
{
    public class ProductsReader
    {
        private static readonly string NoDataMarker1 = "Нет данных";
        private static readonly string NoDataMarker2 = "#Н/Д";
        private static readonly string NoDataMarker3 = "#N/A";

        private readonly string fileName;

        public ProductsReader(string fileName)
        {
            this.fileName = fileName;
        }

        public IEnumerable<ProductRecord> ReadProductRecords()
        {
            using (var fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                {
                    if (!excelReader.Read())
                    {
                        throw new Exception("Products table is empty.");
                    }

                    while(excelReader.Read())
                    {
                        var productRecord = new ProductRecord
                        {
                            Article = ReadCellValue(excelReader, 0),
                            Model = ReadCellValue(excelReader, 1),
                            Brand = ReadCellValue(excelReader, 3),
                            Sex = ReadCellValue(excelReader, 4),
                            MechanismType = ReadCellValue(excelReader, 5),
                            Digits = ReadCellValue(excelReader, 6),
                            Watertight = ReadCellValue(excelReader, 7),
                            WR = ReadCellValue(excelReader, 8),
                            BoxMaterial = ReadCellValue(excelReader, 9),
                            StrapMaterial = ReadCellValue(excelReader, 10),
                            Glass = ReadCellValue(excelReader, 11),
                            TimeDisplayType = ReadCellValue(excelReader, 12),
                            EnergySource = ReadCellValue(excelReader, 13),
                            Diameter = ReadCellValue(excelReader, 14),
                            Weight = ReadCellValue(excelReader, 15),
                            DateDisplay = ReadCellValue(excelReader, 16),
                            Name = ReadCellValue(excelReader, 18),
                            AdditionalFunctions = ReadCellValue(excelReader, 20),
                            Warranty = ReadCellValue(excelReader, 22)
                        };

                        var pictures = ReadCellValue(excelReader, 17);
                        productRecord.Pictures = string.IsNullOrEmpty(pictures) ? new string[0] : pictures.Split(';');

                        yield return productRecord;
                    }
                }
            }
        }

        private string ReadCellValue(IExcelDataReader excelReader, int column)
        {
            var value = excelReader.GetString(column);
            if (string.IsNullOrEmpty(value))
                return null;

            if (value == NoDataMarker1)
                return null;

            if (value == NoDataMarker2)
                return null;

            if (value == NoDataMarker3)
                return null;

            return value;
        }
    }
}
