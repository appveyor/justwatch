﻿using Justwatch.YmlImporter.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Justwatch.YmlImporter.Utilities;

namespace Justwatch.YmlImporter.Delta
{
    public class WatchMapper
    {
        public Watch Map(ProductRecord productRecord)
        {
            var watch = new Watch
            {
                Article = productRecord.Article.RemoveRedundantWhiteSpaces(),
                Name = productRecord.Name.RemoveRedundantWhiteSpaces(),
                Model = productRecord.Model.Replace(productRecord.Brand, string.Empty).RemoveRedundantWhiteSpaces(),
                Pictures = productRecord.Pictures,
                Brand = productRecord.Brand.RemoveRedundantWhiteSpaces(),
                AdditionalFunctions = productRecord.AdditionalFunctions,
                Sex = string.IsNullOrEmpty(productRecord.Sex) ? new WatchSex?() : ParseWatchSex(productRecord.Sex.RemoveRedundantWhiteSpaces()),
                Digits = productRecord.Digits,
                Waterproof = string.IsNullOrEmpty(productRecord.WR) ? string.Empty : productRecord.WR + " атм.",
                StrapMaterial = productRecord.StrapMaterial,
                BoxMaterial = productRecord.BoxMaterial,
                Glass = productRecord.Glass,
                TimeDisplayType = productRecord.TimeDisplayType,
                EnergySource = productRecord.EnergySource,
                DateDisplay = productRecord.DateDisplay,
                Warranty = productRecord.Warranty,
                Weight = string.IsNullOrEmpty(productRecord.Weight) ? new double?() : double.Parse(productRecord.Weight.RemoveRedundantWhiteSpaces(), CultureInfo.InvariantCulture)
            };

            if (!string.IsNullOrEmpty(productRecord.Watertight))
            {
                if (productRecord.Watertight == "Есть" || productRecord.Watertight == "есть")
                    watch.Watertight = true;
                else
                    throw new Exception(string.Format("Не удалось определить являются ли часы водонепроницаемыми: \"{0}\"", productRecord.Watertight));
            }
            
            if (!string.IsNullOrEmpty(productRecord.Diameter))
            {
                var diameter = productRecord.Diameter.RemoveRedundantWhiteSpaces();          
                var sizeParts = diameter.Split('x', 'х');
                if (sizeParts.Length >= 1)
                    watch.Diameter = double.Parse(sizeParts[0], CultureInfo.InvariantCulture);

                if (sizeParts.Length >= 2)
                    watch.Height = double.Parse(sizeParts[1], CultureInfo.InvariantCulture);

                if (sizeParts.Length >= 3)
                    watch.Depth = double.Parse(sizeParts[2], CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(productRecord.MechanismType))
                watch.Mechanism = ParseWatchMechanism(productRecord.MechanismType.RemoveRedundantWhiteSpaces());

            return watch;
        }

        private WatchSex ParseWatchSex(string sex)
        {
            switch (sex)
            {
                case "Для мужчин":
                    return WatchSex.Male;
                case "Для женщин":
                    return WatchSex.Female;
                case "Унисекс":
                    return WatchSex.Unisex;
                case "Детские":
                    return WatchSex.Child;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить пол для часов: \"{0}\".", sex));
            }
        }

        private WatchMechanism ParseWatchMechanism(string mechanism)
        {
            switch (mechanism)
            {
                case "Электронные":
                case "Кварцевые (аккумулятор)":
                    return WatchMechanism.Quartz;
                case "Механические (автоматические)":
                case "Механика с автоподзаводом":
                case "Механика":
                    return WatchMechanism.MechanicalAutomaticWindUp;
                case "Механические (ручной завод)":
                    return WatchMechanism.MechanicalManualWindUp;
                default:
                    throw new NotSupportedException(string.Format("Не удалось определить тип механизма часов: \"{0}\".", mechanism));
            }
        }
    }
}
