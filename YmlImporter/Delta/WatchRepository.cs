﻿using Justwatch.YmlImporter.Domain;
using Justwatch.YmlImporter.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Justwatch.YmlImporter.Delta
{
    public class WatchRepository
    {
        private readonly Dictionary<string, Watch> watches = new Dictionary<string, Watch>();
        private readonly WatchMapper watchMapper = new WatchMapper();

        public WatchRepository(ProductsReader productsDocumentReader)
        {
            foreach (var productRecord in productsDocumentReader.ReadProductRecords())
            {
                var watch = watchMapper.Map(productRecord);
                var brand = StringExtensions.Simplify(watch.Brand);
                var model = StringExtensions.Simplify(watch.Model);               

                // TODO: Resolve watches duplicates
                if (!watches.ContainsKey(brand + model))
                    watches.Add(brand + model, watch);
            }
        }

        public Watch GetWatch(string brand, string model)
        {
            brand = StringExtensions.Simplify(brand);
            model = StringExtensions.Simplify(model);

            if (watches.ContainsKey(brand + model))
                return watches[brand + model];

            return null;
        }
    }
}
