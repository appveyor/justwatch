﻿using Justwatch.YmlImporter.Domain;
using Justwatch.YmlImporter.YmlDocument;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter
{
    public class OfferRecordMapper
    {
        private readonly DescriptionProvider descriptionProvider;
        private readonly BrandPictureRepository brandPictureRepository;

        public OfferRecordMapper(string companyDescription, BrandPictureRepository pictureRepository)
        {
            descriptionProvider = new DescriptionProvider(companyDescription);
            brandPictureRepository = pictureRepository;

            Stock = 1;
        }

        public int Stock { get; set; }

        public OfferRecord Map(Watch watch)
        {
            var offerRecord = new OfferRecord();

            offerRecord.Id = watch.Article;            
            offerRecord.Price = watch.Price;
            offerRecord.Pictures = watch.Pictures;
            offerRecord.Name = watch.Name;
            offerRecord.Available = "true";
            offerRecord.Category = "14324";
            offerRecord.Currency = "RUR";

            offerRecord.Stock = Stock;

            if (!string.IsNullOrEmpty(watch.Brand))
                offerRecord.Parameters.Add("Бренд", watch.Brand);

            if (!string.IsNullOrEmpty(watch.AdditionalFunctions))
                offerRecord.Parameters.Add("Дополнительные функции", watch.AdditionalFunctions);

            if (watch.Sex.HasValue)
            {
                switch (watch.Sex)
                {
                    case WatchSex.Male:
                        offerRecord.Parameters.Add("Пол", "Для мужчин");
                        break;
                    case WatchSex.Female:
                        offerRecord.Parameters.Add("Пол", "Для женщин");
                        break;
                    case WatchSex.Unisex:
                        offerRecord.Parameters.Add("Пол", "Унисекс");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch sex.");
                }
            }

            if (watch.Mechanism.HasValue)
            {
                switch (watch.Mechanism)
                {
                    case WatchMechanism.Quartz:
                        offerRecord.Parameters.Add("Часовой механизм", "Кварцевые (аккумулятор)");
                        break;
                    case WatchMechanism.MechanicalAutomaticWindUp:
                        offerRecord.Parameters.Add("Часовой механизм", "Механические (автоматические)");
                        break;
                    case WatchMechanism.MechanicalManualWindUp:
                        offerRecord.Parameters.Add("Часовой механизм", "Механические (ручной завод)");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch mechanism.");
                }
            }

            if (!string.IsNullOrEmpty(watch.Digits))
                offerRecord.Parameters.Add("Цифры", watch.Digits);

            if (watch.Watertight.HasValue)
            {
                if (watch.Watertight.Value)
                {
                    if (!string.IsNullOrEmpty(watch.Waterproof))
                        offerRecord.Parameters.Add("Водонепроницаемые", watch.Waterproof);
                    else
                        offerRecord.Parameters.Add("Водонепроницаемые", "да");
                }
                else
                {
                    offerRecord.Parameters.Add("Водонепроницаемые", "нет");
                }
            }
            else
            {
                offerRecord.Parameters.Add("Водонепроницаемые", "нет данных");
            }

            if (!string.IsNullOrEmpty(watch.BoxMaterial))
                offerRecord.Parameters.Add("Материал корпуса/футляра", watch.BoxMaterial);

            if (!string.IsNullOrEmpty(watch.StrapMaterial))
                offerRecord.Parameters.Add("Материал браслета", watch.StrapMaterial);

            if (!string.IsNullOrEmpty(watch.Glass))
                offerRecord.Parameters.Add("Стекло", watch.Glass);

            if (!string.IsNullOrEmpty(watch.TimeDisplayType))
                offerRecord.Parameters.Add("Дисплей", watch.TimeDisplayType);

            if (!string.IsNullOrEmpty(watch.EnergySource))
                offerRecord.Parameters.Add("Источник энергии", watch.EnergySource);

            if (watch.Diameter.HasValue)
                offerRecord.Parameters.Add("Размер, мм", GenerateSizeString(watch));

            if (watch.Weight.HasValue)
                offerRecord.Parameters.Add("Вес", watch.Weight.Value.ToString(CultureInfo.InvariantCulture));

            if (!string.IsNullOrEmpty(watch.DateDisplay))
                offerRecord.Parameters.Add("Отображение даты", watch.DateDisplay);

            if (!string.IsNullOrEmpty(watch.Warranty))
                offerRecord.Parameters.Add("Гарантия", watch.Warranty);

            offerRecord.Description = descriptionProvider.GetDescription(CreateDescriptionTags(watch));

            return offerRecord;
        }

        private Dictionary<string, string> CreateDescriptionTags(Watch watch)
        {
            var tags = new Dictionary<string, string>();
            tags.Add("name", watch.Name);

            if (watch.Sex.HasValue)
            {
                switch (watch.Sex)
                {
                    case WatchSex.Male:
                        tags.Add("Пол", "Мужские");
                        break;
                    case WatchSex.Female:
                        tags.Add("Пол", "Женские");
                        break;
                    case WatchSex.Unisex:
                        tags.Add("Пол", "Унисекс");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch sex.");
                }
            }
            else
            {
                tags.Add("Пол", "нет данных");
            }

            tags.Add("Способ отображения времени", watch.TimeDisplayType);
            tags.Add("Цифры", watch.Digits);

            if (watch.Mechanism.HasValue)
            {
                switch (watch.Mechanism)
                {
                    case WatchMechanism.Quartz:
                        tags.Add("Тип механизма", "Кварцевые (аккумулятор)");
                        break;
                    case WatchMechanism.MechanicalAutomaticWindUp:
                        tags.Add("Тип механизма", "Механические (автоматические)");
                        break;
                    case WatchMechanism.MechanicalManualWindUp:
                        tags.Add("Тип механизма", "Механические (ручной завод)");
                        break;
                    default:
                        throw new NotSupportedException("Unknown watch mechanism.");
                }
            }
            else
            {
                tags.Add("Тип механизма", "нет данных");
            }

            tags.Add("Источник энергии", watch.EnergySource);
            tags.Add("Водонепроницаемые", watch.Waterproof);
            tags.Add("Материал корпуса", watch.BoxMaterial);
            tags.Add("Материал браслета/ремешка", watch.StrapMaterial);
            tags.Add("Стекло", watch.Glass);
            tags.Add("Отображение даты", watch.DateDisplay);
            tags.Add("Дополнительные функции", watch.AdditionalFunctions);
            tags.Add("Размер", GenerateSizeString(watch));
            tags.Add("Вес", watch.Weight.HasValue ? watch.Weight.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
            tags.Add("Гарантия", watch.Warranty);
            tags.Add("picture", watch.Pictures[0]);

            var brandPicture = brandPictureRepository.GetBrandPicture(watch.Brand);
            if (brandPicture != null)
            {
                tags.Add("brand picture", brandPicture.Url);
                tags.Add("width", brandPicture.Width.ToString());
                tags.Add("height", brandPicture.Height.ToString());
            }

            foreach (var tag in tags.ToArray())
            {
                if (string.IsNullOrEmpty(tag.Value))
                {
                    tags.Remove(tag.Key);
                    tags.Add(tag.Key, "нет данных");
                }
            }

            return tags;
        }

        private string GenerateSizeString(Watch watch)
        {
            return string.Join("x", new double?[] { watch.Diameter, watch.Height, watch.Depth }
                    .Where(x => x.HasValue)
                    .Select(x => x.Value)
                    .Select(x => x.ToString(CultureInfo.InvariantCulture)));
        }
    }
}
