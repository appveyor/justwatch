﻿using System.Configuration;

namespace Justwatch.YmlImporter
{
    public static class ApplicationSettings
    {
        public static string ProductsWithoutDetails 
        {
            get { return ConfigurationManager.AppSettings["ProductsWithoutDetails"]; }
        }
            
        public static string ProductsWithoutName
        {
            get { return ConfigurationManager.AppSettings["ProductsWithoutName"]; }
        }
        public static string ProductsWithoutPicture
        {
            get { return ConfigurationManager.AppSettings["ProductsWithoutPicture"]; }
        }

        public static string BrandsWithoutFormula
        {
            get { return ConfigurationManager.AppSettings["BrandsWithoutFormula"]; }
        }
        public static string Description
        {
            get { return ConfigurationManager.AppSettings["Description"]; }
        }

        public static string DeltaProducts
        {
            get { return ConfigurationManager.AppSettings["DeltaProducts"]; }
        }

        public static string DeltaPrices
        {
            get { return ConfigurationManager.AppSettings["DeltaPrices"]; }
        }

        public static string BestWatchProducts
        {
            get { return ConfigurationManager.AppSettings["BestWatchProducts"]; }
        }

        public static string Error
        {
            get { return ConfigurationManager.AppSettings["Error"]; }
        }

        public static string Pricing
        {
            get { return ConfigurationManager.AppSettings["Pricing"]; }
        }

        public static string BrandPictures
        {
            get { return ConfigurationManager.AppSettings["BrandPictures"]; }
        }
    }
}
