﻿using Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter
{
    public class BrandPicture
    {
        public string Brand { get; set; }
        public string Url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class BrandPictureRepository
    {
        private readonly string fileName;
        private Dictionary<string, BrandPicture> brandPictures;

        public BrandPictureRepository(string fileName)
        {
            this.fileName = fileName;
        }

        public BrandPicture GetBrandPicture(string brand)
        {
            if (brandPictures == null)
                brandPictures = ReadBrandPictures().ToDictionary(x => x.Brand.ToUpper(), x => x);

            if (!brandPictures.ContainsKey(brand.ToUpper()))
                throw new Exception(string.Format("No brand picture for {0}.", brand));

            return brandPictures[brand.ToUpper()];
        }

        private IEnumerable<BrandPicture> ReadBrandPictures()
        {
            using (var fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream))
                {
                    excelReader.IsFirstRowAsColumnNames = true;
                    excelReader.Read();

                    while (excelReader.Read())
                    {
                        var brand = excelReader.GetString(0);
                        if (brand == null)
                            continue;

                        var url = excelReader.GetString(1);
                        var width = excelReader.GetInt32(2);
                        var height = excelReader.GetInt32(3);

                        yield return new BrandPicture
                        {
                            Brand = brand,
                            Url = url,
                            Width = width,
                            Height = height
                        };
                    }
                }                
            }
        }
    }
}
