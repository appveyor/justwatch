﻿using System.Collections.Generic;

namespace Justwatch.YmlImporter.Domain
{
    public interface IWatchProvider
    {
        IEnumerable<Watch> ImportWatches();
    }
}
