﻿namespace Justwatch.YmlImporter.Domain
{
    public enum WatchMechanism
    {
        MechanicalAutomaticWindUp,
        MechanicalManualWindUp,
        Quartz
    }
}
