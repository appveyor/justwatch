﻿using System.Collections.Generic;
using System.Linq;
using Justwatch.YmlImporter.YmlDocument;
using Justwatch.YmlImporter.Domain;

namespace Justwatch.YmlImporter.Interfaces
{
    public class CompositeWatchProvider : IWatchProvider
    {
        private IEnumerable<IWatchProvider> watchProviders;

        public CompositeWatchProvider(params IWatchProvider[] watchProviders)
        {
            this.watchProviders = watchProviders;
        }

        public IEnumerable<Watch> ImportWatches()
        {
            return watchProviders.SelectMany(provider => provider.ImportWatches());
        }
    }
}
