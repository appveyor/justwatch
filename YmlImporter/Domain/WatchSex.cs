﻿namespace Justwatch.YmlImporter.Domain
{
    public enum WatchSex
    {
        Male,
        Female,
        Unisex,
        Child
    }
}
