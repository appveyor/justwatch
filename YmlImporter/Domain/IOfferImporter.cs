﻿using Justwatch.YmlImporter.YmlDocument;
using System.Collections.Generic;

namespace Justwatch.YmlImporter.Interfaces
{
    public interface IOfferImporter
    {
        IEnumerable<OfferRecord> ImportOffers();
    }  
}
