﻿namespace Justwatch.YmlImporter.Console
{
    public class ImportParametersParser
    {
        private const string StockParameter = "-stock";
        private const string MaxPriceParameter = "-maxprice";

        public ImportParameters Parse(string[] args)
        {
            var importParameters = new ImportParameters();

            for (var i = 0; i < args.Length - 1; i+=2)
            {
                Parse(importParameters, args[i], args[i + 1]);
            }

            return importParameters;
        }

        private void Parse(ImportParameters parameters, string paramName, string paramValue)
        {
            switch (paramName)
            {
                case StockParameter:
                    parameters.Stock = int.Parse(paramValue);
                    break;
                case MaxPriceParameter:
                    parameters.MaxPrice = int.Parse(paramValue);
                    break;
            }
        }
    }
}
