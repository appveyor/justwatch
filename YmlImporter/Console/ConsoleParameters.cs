﻿namespace Justwatch.YmlImporter.Console
{
    public class ImportParameters
    {
        public int? Stock { get; set; }
        public int? MaxPrice { get; set; }
    }
}
