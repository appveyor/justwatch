﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Justwatch.YmlImporter.Utilities
{
    public static class StringExtensions
    {
        public static string Simplify(this string inputString)
        {
            return inputString.Replace("_", string.Empty).Replace(" ", string.Empty).ToUpper();
        }
        
        public static string ToFirstLetterUppercase(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string RemoveRedundantWhiteSpaces(this string input)
        {
            var output = new StringBuilder();

            char? previousChar = null;
            int charIndex = -1;

            foreach (var @char in input)
            {
                charIndex++;

                if (char.IsWhiteSpace(@char))
                {
                    if (!previousChar.HasValue)
                        continue;

                    if (char.IsWhiteSpace(previousChar.Value))
                        continue;

                    if (charIndex + 1 == input.Length)
                        continue;
                }

                output.Append(@char);
                previousChar = @char;                
            }

            return output.ToString();
        }
    }
}
