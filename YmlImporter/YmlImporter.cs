﻿using Justwatch.YmlImporter.Domain;
using Justwatch.YmlImporter.Pricing;
using Justwatch.YmlImporter.YmlDocument;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace Justwatch.YmlImporter
{
    public class YmlImporter
    {
        private readonly PriceCalculator priceCalculator = new PriceCalculator();
        private readonly BrandPictureRepository brandPictureRepository;

        public YmlImporter(Dictionary<string, string> brandFormulas, string companyDescription, BrandPictureRepository pictureRepository)
        {
            priceCalculator.LoadBrandFormulas(brandFormulas);
            CompanyDescription = companyDescription;
            brandPictureRepository = pictureRepository;

            ProductsWithoutName = new Collection<string>();            
            ProductsWithoutPicture = new Collection<string>();
            ProductsWithHigherPrice = new Collection<string>();
            BrandsWithoutCalculatedPrice = new Collection<string>();

            Stock = 1;
        }

        public int Stock { get; set; }
        public int? MaxPrice { get; set; }
        public string CompanyDescription { get; private set; }
        public int ImportedOffers { get; private set; }
        public Collection<string> ProductsWithoutName { get; private set; }
        public Collection<string> ProductsWithoutPicture { get; private set; }
        public Collection<string> ProductsWithHigherPrice { get; private set; }
        public Collection<string> BrandsWithoutCalculatedPrice { get; private set; }

        public void Import(
            IWatchProvider watchProvider,
            string ymlDocumentPath)
        {
            ImportedOffers = 0;
            ProductsWithoutName.Clear();
            ProductsWithoutPicture.Clear();
            ProductsWithHigherPrice.Clear();
            BrandsWithoutCalculatedPrice.Clear();

            var offerRecordMapper = new OfferRecordMapper(CompanyDescription, brandPictureRepository);
            offerRecordMapper.Stock = Stock;

            using (var ymlDocument = new YmlDocumentWriter(ymlDocumentPath))
            {
                ymlDocument.WriteHeader();

                foreach (var watch in watchProvider.ImportWatches())
                {
                    if (!CheckWatch(watch))
                        continue;

                    if (!CalculatePrice(watch))
                        continue;

                    var offerRecord = offerRecordMapper.Map(watch);               
                    
                    
                    ymlDocument.WriteOffer(offerRecord);

                    ImportedOffers++;
                }

                ymlDocument.WriteFooter();
            }
        }

        private bool CalculatePrice(Watch watch)
        {
            var calculatedPrice = priceCalculator.CalculatePrice(watch.Brand, watch.Price);

            if (calculatedPrice.HasValue)
            {
                watch.Price = Math.Floor(calculatedPrice.Value);
                return true;
            }
            else
            {
                if (!BrandsWithoutCalculatedPrice.Contains(watch.Brand))
                    BrandsWithoutCalculatedPrice.Add(watch.Brand);
                return false;
            }    
        }

        private bool CheckWatch(Watch watch)
        {
            if (string.IsNullOrEmpty(watch.Article))
            {
                return false;
            }

            if (string.IsNullOrEmpty(watch.Name))
            {
                ProductsWithoutName.Add(watch.Article);
                return false;
            }

            if (watch.Pictures.Length == 0 ||
                watch.Pictures.All(picture => string.IsNullOrEmpty(picture)))
            {
                ProductsWithoutPicture.Add(watch.Name);
                return false;
            }

            if (MaxPrice.HasValue && watch.Price > MaxPrice.Value)
            {
                ProductsWithHigherPrice.Add(watch.Name);
                return false;
            }

            return true;
        }
    }
}
